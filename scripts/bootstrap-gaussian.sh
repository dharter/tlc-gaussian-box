#!/usr/bin/env bash
export mntpnt="/vagrant"
export packages="${mntpnt}/packages"
export scripts="${mntpnt}/scripts"
export documents="${mntpnt}/documents"
export g09root="/opt"
export g09scratch="/home/scratch"
gaussian_tarball="${packages}/E64-930N.tgz"
gv_tarball="${packages}/gv5.tar.bz2"
gaussian_user="vagrant"
gaussian_group="vagrant"
package_url="http://hostname.com/packages"

# download the packages if needed
# wget -c ${package_url}/E64-930N.tgz /vagrant/packages
# wget -c ${package_url}/gv5.tar.bz2 /vagrant/packages
if [ ! -f ${gaussian_tarball} ] || [ ! -f ${gv_tarball} ]
then
    echo "Missing Gaussian 09 or Gauss View 05 installation packages."
    echo "Contact your TLC system administrator to obtain the packages."
    echo "Gaussian 09 / Gauss View 05 will not be installed, but these"
    echo "can be installed post provisioning."
    exit 0
fi


# extract the gaussian source/binary files for installation
cd ${g09root}
tar xvfz ${gaussian_tarball}
chown -R ${gaussian_user}:${gaussian_group} ${g09root}/g09

tar xvfj ${gv_tarball}
chown -R ${gaussian_user}:${gaussian_group} ${g09root}/gv

# perform install script
cd ${g09root}/g09
./bsd/install
# need to do again to ensure all files installed previously are changed to be owned
# by gaussian user
chown -R ${gaussian_user}:${gaussian_group} ${g09root}/g09

# we may want to configure some different scratch?
# given our current provisioning setup, there won't be much
# space available in root / directory for scratch, so lets set up
# a directory in gaussian home directory for scratch files
mkdir -p ${g09scratch}
chown -R ${gaussian_user}:${gaussian_group} ${g09scratch}

# set up needed user environment variables to be available on login
config="/home/${gaussian_user}/.bashrc"
#cp --backup=numbered ${config} ${config}.orig
cat >> ${config} <<EOF

# usefel aliases
alias d='ls -lh'

# set up environment variables needed by gaussian process to run
# and source the g09.profile file for additional settings for gaussian.
export g09root="${g09root}"
export GAUSS_SCRDIR="${g09scratch}"
export GAUSS_LFLAGS=' -vv -opt "Tsnet.Node.lindarsharg: ssh"'
source ${g09root}/g09/bsd/g09.profile
EOF

# copy local gaussian scripts for use with testing and using gaussian
for script in g09serial round-floats run-all-tests
do
    cp "${scripts}/${script}" "${g09root}/g09/${script}"
    chmod 754 "${g09root}/g09/${script}"
    dos2unix "${g09root}/g09/${script}"
done
chown -R ${gaussian_user}:${gaussian_group} ${g09root}/g09

# copy documentation/user manuals to desktop for easy access
mkdir -p /home/${gaussian_user}/Desktop
for doc in ${documents}/*
do
    filename=`basename "${doc}"`
    cp "${doc}" "/home/${gaussian_user}/Desktop"
done
chown -R ${gaussian_user}:${gaussian_group} /home/${gaussian_user}


# install mesa-libGLU for GaussView
apt install -y libglu1-mesa libglu1-mesa-dev

# setup desktop launcher for GaussView
cat > ${g09root}/gv/gview.sh <<EOF
#!/usr/bin/env bash
export GAUSS_BSDDIR=${g09root}/g09/bsd
export GAUSS_LEXEDIR=${g09root}/g09/linda-exe
export GAUSS_ARCHDIR=${g09root}/g09/arch
export G09BASIS=${g09root}/g09/basis
export GAUSS_EXEDIR=${g09root}/g09/bsd:${g09root}/g09/local:${g09root}/g09/extras:${g09root}/g09
export g09root=${g09root}
export GV_DIR=${g09root}/gv
EOF

cat >> ${g09root}/gv/gview.sh <<"EOF"
source ${GV_DIR}/bin/gv_ld_env.sh
"${GV_DIR}/gview.exe" $*
EOF
chown ${gaussian_user}:${gaussian_group} ${g09root}/gv/gview.sh
chmod ug+x ${g09root}/gv/gview.sh
cp /home/${gaussian_user}/Desktop/GaussView.desktop /usr/share/applications
chmod -x /usr/share/applications/GaussView.desktop

# set link/ld dynamic libraries for gv 5
config="/etc/ld.so.conf.d/gaussian.conf"
cat > ${config} <<EOF
/opt/g09/bsd
/opt/g09/local
/opt/g09/extras
/opt/g09
/opt/g09/linda8.2/opteron-linux/lib/
/opt/gv/lib
EOF
ldconfig
