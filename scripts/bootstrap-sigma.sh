#!/usr/bin/env bash
export mntpnt="/vagrant"
export packages="${mntpnt}/packages"
export scripts="${mntpnt}/scripts"
export documents="${mntpnt}/documents"
export sigmaroot="/opt"
sigma_tarball="${packages}/sigma_dec13.zip"
gaussian_user="vagrant"
gaussian_group="vagrant"
package_url="http://hostname.com/packages"

# download the packages if needed
# wget -c ${package_url}/sigma_dec13.zip /vagrant/packages
if [ ! -f ${gaussian_tarball} ] || [ ! -f ${gv_tarball} ]
then
    echo "Missing Sigma installation packages."
    echo "Contact your TLC system administrator to obtain the packages."
    echo "Sigma software will not be installed, but these"
    echo "can be installed post provisioning."
    exit 0
fi

# extract the source
cd ${sigmaroot}
unzip ${sigma_tarball}
cd ${sigmaroot}/sigma

# create a small makefile to better reproduce build
cat > Makefile <<EOF
# build sigma program from single fortran source
sigma: sigma_dec13.f
	gfortran -O2 -unroll sigma_dec13.f -o sigma

test:
	./sigma -i c60.mol2 -h sca.par

install:
	cp sigma /usr/local/bin

clean:
	rm -rf sigma
EOF

# build and test
make clean
make
make test
make install
ldconfig

