#!/usr/bin/env bash
#
# Using these instructions to build/install: https://www.webmo.net/support/gamess_linux.html

export mntpnt="/vagrant"
export packages="${mntpnt}/packages"
export gamess_root="/opt"
export gamess_tarball="${packages}/gamess-current.tar.gz"
export gaussian_user=vagrant
package_url="http://hostname.com/packages"

# download the packages if needed
# wget -c ${package_url}/gamess-current.tar.gz /vagrant/packages
if [ ! -f ${gamess_tarball} ]
then
    echo "Missing Gamess computational chemistry installation packages."
    echo "Contact your TLC system administrator to obtain the packages."
    echo "Gamess will not be installed, but can be installed post provisioning."
    exit 0
fi


# install some needed prerequisites, especially gfortran compiler and openblas development libraries
#yum install -y csh gcc-gfortran patch openblas openblas-devel openblas-static wxmacmolplt
apt install -y csh gfortran patch libopenblas-base libopenblas-dev

# extract the Gamess source/binary files for installation
cd ${gamess_root}
tar xvfz ${gamess_tarball}
chown -R root:root gamess
chmod -R g-s gamess
cd ${gamess_root}/gamess

# configure the installation, this config is interactive so we are echoing the answers to the
# config questions needed for our system setup/configuration
echo -e " 
linux64
${gamess_root}/gamess
${gamess_root}/gamess
00
gfortran
7.5

openblas
/usr/lib/x86_64-linux-gnu


sockets
no
no
no

" | ./config

# compile ddi
cd ddi
yes | ./compddi >& compddi.log
mv ddikick.x ..
cd ..

# compile gamess (may take some time)
yes | ./compall >& compall.log

# link Gamess
yes | ./lked gamess 00 >& lked.log

# update the rungms script for our system
cp rungms rungms.bck
sed -i 's|set SCR=.*|set SCR=/home/scratch|g' rungms
sed -i 's|set USERSCR=.*|set USERSCR=/tmp|g' rungms
sed -i 's|set GMSPATH=.*|set GMSPATH=/opt/gamess|g' rungms

# add Gamess to path so can find and run rungms script
cat >> /home/${gaussian_user}/.bashrc <<EOF

# Add Gamess executable to end of user path 
export PATH=\${PATH}:${gamess_root}/gamess
EOF

# can check compilation and linking for errors
# grep "cannot stat" compall.log
# tail -n 20 ddi/compddi.log
# more lked.log

# run a test job as vagrant user
# cd ~
# mkdir gamess
# cp -p /opt/gamess/tests/standard/exam01.inp ~/gamess
# cd ~/gamess
# /opt/gamess/rungms exam01 > exam01.log


# install MacMolPlt
# add repo so we can do an apt install
echo "deb [trusted=yes] https://dl.bintray.com/brettbode/Xenial xenial main" > /etc/apt/sources.list.d/wxmacmolplt.list
apt update
apt install -y wxmacmolplt
# had a problem where if ldconfig was not run, caused traced service to crash?
ldconfig 
