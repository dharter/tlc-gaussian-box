#!/usr/bin/env bash
export mntpnt="/vagrant"
export packages="${mntpnt}/packages"
private_key="${packages}/tlckey.pem"
public_key="${packages}/tlckey.pub"
gaussian_user="vagrant"
gaussian_group="vagrant"
gaussian_cluster_user=`cat ${packages}/gaussian-cluster-user.txt | tr -d [:space:]`
gaussian_cluster_port="12086"

# test if keys present to configure gaussian compute cluster access
if [ ! -f ${private_key_file} ] || [ ! -f ${public_key_file} ]
then
    echo "Missing TLC public/private ssh security keys."
    echo "Contact your TLC system administraotr to obtain your ssh keys."
    echo "SSH access to gaussian compute cluster will not be configured."
    exit 0
fi


# copy ssh keys and set correct permissions to use as ssh identities
ssh_directory="/home/${gaussian_user}/.ssh"
mkdir -p ${ssh_directory}
chmod -R go-rwx ${ssh_directory}
cp ${public_key} ${ssh_directory}/id_rsa.pub
chmod 644 ${ssh_directory}/id_rsa.pub
cp ${private_key} ${ssh_directory}/id_rsa
chmod 600 ${ssh_directory}/id_rsa
chown -R ${gaussian_user}:${gaussian_group} ${ssh_directory}

config="${ssh_directory}/config"
cat >> ${config} <<EOF
Host *
     ServerAliveInterval 300
     ServerAliveCountMax 3
     StrictHostKeyChecking no

Host gauss gauss-cluster gaussian-cluster
    Hostname tlc.harter.pro
    User ${gaussian_cluster_user}
    Port ${gaussian_cluster_port}
    IdentityFile ~/.ssh/id_rsa
    IdentitiesOnly yes
EOF
chown -R ${gaussian_user}:${gaussian_group} ${ssh_directory}

# add to root user as well so we can mount for user on boot
ssh_directory="/root/.ssh"
mkdir -p ${ssh_directory}
chmod -R go-rwx ${ssh_directory}
cp ${public_key} ${ssh_directory}/id_rsa.pub
chmod 644 ${ssh_directory}/id_rsa.pub
cp ${private_key} ${ssh_directory}/id_rsa
chmod 600 ${ssh_directory}/id_rsa
chown -R root:root ${ssh_directory}

config="${ssh_directory}/config"
cat >> ${config} <<EOF
Host *
     ServerAliveInterval 300
     ServerAliveCountMax 3
     StrictHostKeyChecking no

Host gauss gauss-cluster gaussian-cluster
    Hostname tlc.harter.pro
    User ${gaussian_cluster_user}
    Port ${gaussian_cluster_port}
    IdentityFile ~/.ssh/id_rsa
    IdentitiesOnly yes
EOF
chown -R root:root ${ssh_directory}


# add mount point to file system table file to try and mount the sshfs gaussian cluster
# directory for the user
mkdir -p /home/${gaussian_user}/gauss-cluster
config=/etc/fstab
cat >> ${config} <<EOF

# try and mount gaussian cluster home directory for user to /home/vagrant/gauss-cluster
${gaussian_cluster_user}@gaussian-cluster:/home/${gaussian_cluster_user} /home/${gaussian_user}/gauss-cluster fuse.sshfs uid=1000,gid=1000,allow_other,default_permissions,umask=022 0 0

EOF
yes | mount -a


# while we are at it, set up link for /vagrant files for the user as gauss-local
su -c "ln -s /vagrant /home/${gaussian_user}/gauss-local" vagrant
#chown ${gaussian_user}:${gaussian_group} /home/${gaussian_user}/gauss-local
