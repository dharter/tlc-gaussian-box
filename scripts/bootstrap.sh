#!/usr/bin/env bash
#
# Before first vagrant up, check that "vagrant plugin install vagrant-vbguest" is installed,
# seems to be some issues with centos/7 images and the vbguest additions

# make sure initially everything up to date
apt -y update
apt -y dist-upgrade

# set time zone and hostname
timedatectl set-timezone America/Chicago
hostnamectl set-hostname gaussbox


apt -y install build-essential csh wget htop sshfs python dos2unix

# remove password for vagrant user
passwd -d vagrant

