# Gaussian Compute Virtual Box

This repository contains a complete bootstrap install of a local Gaussian 09
computational chemistry virtual boxen.  The Gaussian 09 distribution along
with GaussView 5 are installed in a Virtual Box using Vagrant tools for
provisioning.  Additional computational chemistry tools and materials are
also preloaded into the virtual box as part of the bootstrap provisioning
of the vagrant box.


## Getting Started

A video tutorial/walkthrough of these installation steps and using the
GaussBox can be found [here](https://youtu.be/OyY-1gF32C4).

Three prerequisite tools are needed to be installed on your system before
you can bring up the virtual box: git, VirtualBox and Vagrant. In general, no matter which OS you have on your personal system, you need
to perform the following steps:

1. Install [git](https://git-scm.com/)
distributed revision control system tools if they are not
already available on your personal computing system.
2. Install Oracle's [VirtualBox](https://www.virtualbox.org/)
open source virtualization solution on your system to run the
Vagrant virtual server box.
3. Install the [Vagrant](https://www.vagrantup.com/)
virtualization management tools.
3. Clone this course's repository to your system using `git clone`.
4. Use `vagrant up` to boot up and provision the Python stack
and JupyterHub server vagrant box.

The first 3 steps are system dependent, but should only require use of
a standard installer to accomplish.  Once you have git, VirtualBox
and Vagrant on your  system, you should be able to clone this
repository, start the vagrant virtual box and provision it, and then
run and access your Gaussian compute box locally through a standard
Linux desktop GUI.

#### 1. Download and Install git client

If you are on MacOS or Linux you most likely have git already installed.

**Windows OS Instructions**

- Download the git installer for Windows from the official
[git-scm Downloads](https://git-scm.com/download/win)
site that develops the git tools.
- This is a standard windows installer, so once downloaded run it.  You can
accept most all of the default suggestions.  However, for the setting on how
to handle newlines, choose the third option to keep newlines as they are
instead of doing any conversion.  There can be issues where scripts are
pulled from the repository, then converted to use Windows newlines.  But then
these scripts will fail mysteriously when we try to run in a Unix/Linux box
expecting Unix/Linux newlines for the script files.
- After installing, open a
[command line terminal](https://www.computerhope.com/issues/chusedos.htm)
and test that you have the git command line tool available:
```
C:\Users\username> where git
C:\Program Files\Git\cmd\git.exe
```
```
C:\Users\username> git --version
git version 2.27.0.windows.1
```
If you instead see the message `INFO: Could not find files for the given patern(s).` then git was either not installed, or it was not added to your
[PATH environment variable](https://www.computerhope.com/issues/ch000549.htm)
correctly.  Make sure git is installed and runnable
from the command line before proceeding to the next step.

**MacOS and Linux Instruction**

Git should most likely already be installed on your system by default.
To test, open a terminal and try finding the `git` command:
```
$ which git
/usr/bin/git
```
```
$ git --version
git version 2.25.1
```

On MacOS X if you need to install Git, you can go to the link above for
the Windows OS and download the git installer for MacOS.  This is a
standard MacOS based app installer.  Alternatively I recommend installing
the [brew package manager](https://brew.sh/),
which adds open source package management to a
standard MacOS system.  With brew installed, you can use it to install git:

```
$ sudo brew install git
```

On a Linux OS system, use your distributions package manager to
install the git package.  This is most likely either the `apt` or `yum`
command depending on which distribution of Linux you are using:
```
# debian based systems like Ubuntu use apt package manager
$ sudo apt install git
```
```
# Fedora based systems like CentOS likely use yum
$ sudo yum install git
```

#### 2. Install VirtualBox Virtualization Tools

You will need to install VirtualBox on your system to use the vagrant box
setup given to you for this class.  A standard app installer/package is provided
on the [VirtualBox Downloads](https://www.virtualbox.org/wiki/Downloads)
site for Windows, MacOS X and Linux distributions.  Download the
installer and install it in the standard way for your operating system.
You should be able to accept the defaults offered by the installer for your
system.

Once installed, a graphical interface for VirtualBox is available that you
should be able to search for from your start menu and run.  Also a command
line tool should be installed and working.  Open a command line terminal
or shell on your system and test that the `VBoxManage` command is available
and in your path.  On Linux and MacOS X it should be installed in a standard
location that will be on your usual system path:
```
# on MacOX and Linux use the which command, on Windows use where instead
# MacOS / Linux
$ which VBoxManage
/usr/bin/VBoxManage
$ VBoxManage --version
6.1.12r138449
```

On Windows OS the installer does not add the location of the VirtualBox command
line tools to your PATH.  You can either add `C:\Program Files\Oracle\VirtualBox` to your PATH, or you can change to this directory or specify the full path name to run the `VBoxManage.exe` tool from the command
line:
```
# Windows
> "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" --version
6.1.12r138449
```
The location of the PATH where the executable resides and the version
number may differ slightly.  As of the writing of this README,
you should have at least VirtualBox version 6.1.12 or higher installed on
your system.


#### 3. Install Vagrant Virtualization Management Tools

Vagrant greatly simplifies managing and spinning up virtual box instances.
You have been given a Vagrantfile in the class repository that should
spin up and provision a full Python Stack vagrant box running a JupyterHub
server.

Again standard install packages are provided on the
[Vagrant Download](https://www.vagrantup.com/downloads)
site for Windows, MacOS X and Linux systems.
Download the installer or package appropriate for your system and
install it.  Accept the suggested defaults for all questions for the install.
Both VirtualBox and Vagrant may require a system reboot (especially on Windows),
so once you have Vagrant successfully installed, it is a good idea to reboot
your system at this point before continuing.

**Reboot System**

When you reboot your system to make sure that the VirtualBox and the
Vagrant installs completely take effect, it would be a good idea to also
check your BIOS settings and make sure that you have hardware virtualization
enabled.  VirtualBox and Vagrant will not work if hardware virtualizaiton
is not enabled.  This is probably enabled now adays by default?  but best
to explicitly check it.

When booting up, enter your system BIOS

- [How to Enter the BIOS on Any PC: Access Keys by Manufacturer](https://www.tomshardware.com/reviews/bios-keys-to-access-your-firmware,5732.html)

Usually the `F2` key should work, but if not there should usually be
a message on your first boot screen telling you what the BIOS access
key is.

In your BIOS, find the setting for hardware Virtualization

- [Enabling Virtualization in your PC BIOS](https://bce.berkeley.edu/enabling-virtualization-in-your-pc-bios.html)

This setting is usually in your BIOS menu under the Processor or CPU settings.
And as the link says, it will probably be called either `VT-x` or  `AMD-V`.
Make sure it is enabled.

**After Reboot**

Again once installed it is a good idea to confirm that the command line
tools are available before moving on.  Since `vagrant` is mainly intended
to be used from the command line, this time if you are a Windows user you should
find that the command has been added to your PATH for you.

To test, use the `where` or `which` command, and try running `vagrant` asking
for the installed version.  If the command cannot be found, you need to
stop and check your install and insure your PATH is set correctly.

```
# MacOS / Linux use which to see if vagrant tool in PATH
$ which vagrant
/usr/bin/vagrant
# WindowsOS use where instead
> where vagrant
C:\HashiCorp\Vagrant\bin\vagrant.exe
```
```
# if it is on your path, test you can run it by determining which version you are on
$ vagrant --version
Vagrant 2.2.9
```
As of the writing and testing of this README, you should have installed Vagrant
version 2.2.9 or higher on your system now.

**NOTE Windows Users** Also you might want to check and disable Hyper-V.  Again
I think this is usually disabled by default (unless you have installed
something like Docker).  You can check and disable using the following.

- [Virtualization applications do not work together with Hyper-V](https://support.microsoft.com/en-us/help/3204980/virtualization-applications-do-not-work-together-with-hyper-v-device-g)


#### 4. Clone the Gaussian Compute Box Git Repository

At this point you should successfully have all of the tools you need for the
class installed.  We will now use them to clone the repository and
create and provision your Gaussian virtual compute box.

The clone step is easy.  You can use graphical git tools if you are more
familiar with them, though you can use git from the command line
terminal as well.

To clone the repository from the command line, change to the directory you
want to clone a copy of the files into, then run the git command. I
usually prefer to have a directory named `boxes` in my home directory where I
keep all of my virtual/vagrant box instances.  On Windows, MacOS and Linux you can use `mkdir`,
`cd`, and the `git` command to accomplish this from a command line terminal
or shell:
```
# create a directory called boxes, this assumes you are currently in your home directory
$ mkdir boxes
```
```
# change into the boxes directory to be your current working subdirectory
$ cd boxes
```
```
# clone the Gaussian boxen repository into your current directory
$ git clone https://bitbucket.org/dharter/tlc-gaussian-box.git
```

The clone is basically like a download of the files.  If you are in the
boxes directory, there will now be a new subdirectory named `tlc-gaussian-box`
that contains all of the files for the class that were just downloaded.

#### 5. Copy Installation Packages and User Keys, Set Username

Before you begin the provisioning/install in the next step, obtain
the necessary installation packages and your user keys from your
TLC administrator.  You should place all install packages and user
keys into the `packages` subdirectory that you will have inside of
the `tlc-gaussian-box` directory you just cloned using git.

The following packages are currently installed by default:

- Gaussian 09: package name `E64-930N.tgz`
- Gauss View 05: Package name `gv5.tar.bz2`
- Gamess: Package name `gamess-current.tar.gz`

In addition to any install packages, also place your public and private
key into the packages directory.  These will probably be named
`tlckey.pub` and `tlckey.pem` respectively when sent to you by
your TLC administrator.  Finally make sure that you edit the file
named `gaussian-cluster-user.txt` in the packages directory.  Replace
the username `centos` with your actual Gaussian Computer cluster
username given to you by your TLC administrator.

#### 6. Start and Provision the Gaussian compute box

Assuming you just completed the previous steps 4 and 5, you are now in the `boxes`
directory, and you just cloned a copy of the `tlc-gaussian-box` directory.
You have also obtained your install packages and your user keys and put
them into the `packages` subdirectory.
Change into the class repository directory from the command line:
```
$ cd tlc-gaussian-box
```

In the next step we will attempt to start and provision your vagrant box.
The box will download and start a new virtual machine, install the
Gaussian 09 and GaussView software, along with other tools.
It will create a virtual box instance with a GUI dekstop and start
the virtual box running.

To start the vagrant box and have it provision itself run the following command
from a terminal in your `tlc-gaussian-box` repository directory:
```
$ vagrant up
```

This step will take some time.  On my rural Texas DSL I tend to get 100 to
500 k/sec download when I run this.  The base CentOS `virtualbox.box` image will
first be downloaded.  This usually takes about 30-60 minutes or so from my
home to complete.  If the base image successfully downloads, the installer
will then attempt to install the desktop, Gaussian and the other tools.
This will again take probably 30-60 minutes depending on your speed.

If no errors occurs, the last thing you will see when the install finishes is a message from our Bootstrap provisioning script:
```
...
Gaussian compute boxen successfully installed, desktop running!
```

If you instead see error messages in your terminal at the end, please copy them
and e-mail them to your instructor for advice on proceeding.

At this point, assuming no errors, your vagrant box instance is actually
up and running.  However it is a good idea to shutdown the server and
bring it back up at this point to test that things are working cleanly
and as expected.  The box should shutdown itself after successfully
doing the initial provisioning/installations.  If it doesn't, you should
halt it by hand by doing the following:

```
$ vagrant halt
==> default: Unmounting NFS shared folders from guest...
==> default: Forcing shutdown of VM...
```

You should see that the box is shutdown and not get any error messages.
Then bring the box back up again.  Once the base image is downloaded,
and the box is provisioned and set up, bringing it up again should be
relatively quick.  It will boot in a minute or less usually.

```
$ vagrant up
Bringing machine 'default' up with 'virtualbox' provider...


```

Things to check here.  You might get some warnings, like your base box is
out of date, or other things.  Usually warnings are not issues you need to
worry about, but if you have a question send me the warning message.

What you want to ensure here includes:

- You should see that port 22 on the guest is forwarded to some port on your
  host.  This provides ssh access into your guest machine if needed.
- Hopefully you will see that the GuestAdditions are running ok.  These
  can sometimes have problems.  
- Finally your repository should be mounted from your host machine to
  your guest.  This is the line that says
  ```
  default: /vagrant => /home/dash/boxes/tlc-gaussian-box
  ```
  The location of the directory on your host will differ, but it should be
  the location of the repository you cloned in step 4 above.  This
  sharing and mounting of your repository directory allows you to access
  and open the files from the GUI desktop inside of the vagrant box.

If you see that the machines is booted and ready! now, you can then
proceed to trying to log in and use your Gaussian virtual box.


## Using your Gaussian Virtual Boxen

If your vagrant box provisions itself and boots up correctly, it will
be running a standard Linux desktop with Gaussian 09, GaussView 05 and other
tools installed and available to use.  

The system is configured with a standard username.  No password is
set for this user, so you should be able to log in directly without
providing any password:

**username:** vagrant



#### Contact

`derek dot harter @ tamuc dot edu`
