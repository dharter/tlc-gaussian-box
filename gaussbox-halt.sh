#!/usr/bin/env bash

# need to change the following if in fact your box doesn't live here
BOX_HOME=~/boxes/tlc-gaussian-box
cd ${BOX_HOME}
vagrant halt
