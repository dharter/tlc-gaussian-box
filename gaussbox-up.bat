REM Simple enough, bat files start with CWD in the directory location where the bat file is located.
REM So simply perform a vagrant up to bring up the gauss box here.
REM
REM To add a desktop shortcut: https://winaero.com/blog/pin-a-batch-file-to-the-start-menu-or-taskbar-in-windows-10/
REM 1. Right click on the Desktop and select "New->Shortcut"
REM 2. Set the command to run as `cmd /c "c:\Users\username\boxes\tlc-gauss-box\gaussbox-up.bat"`
REM 3. Change the default directory to run in as "c:\Users\username\boxes\tlc-gauss-box"
REM 4. Can also select an icon, e.g. use icon from VirtualBox.exe
REM 5. Right click on the desktop shortcut and select "Pin to Taskbar" if want on taskbar or "Pin to Start" for start menu
vagrant up
